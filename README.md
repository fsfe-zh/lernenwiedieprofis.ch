# lernenwiedieprofis.ch source

This repo contains the source for the [lernenwiedieprofis.ch](lernenwiedieprofis.ch) website.

The website is the main tool in our "Lernen wie die Profis" campaign which promotes the use of Free Software in schools and universities.

The website is a static webpage which we generate from markdown files using the [hugo static site generator](https://gohugo.io/). The `hugo` theme which we use is called `terrassa`.

## Contributing

**First**, get a local clone of this repository with the `terrassa` hugo theme submodule, with

`git clone --recurse-submodules https://git.fsfe.org/fsfe-zh/lernenwiedieprofis.ch.git`

If you already have a local repository and want to update it to the latest remote source, use `git pull` and `git submodule update --remote`.

**Second**, make the changes to the source files you would like to make.

**Third**, run `hugo server` and inspect the resulting, locally hosted static website.

**Fourth**, if you're happy with your changes, commit them in your local repository first. If you're adapting the markdown files containing the website's content, the standard `git add` and `git commit` operations in the root git directory (`lernenwiedieprofis.ch`) should suffice. However, if you're adapting the theme in `/themes/terrassa`, you'll need to do two things:

1. Navigate into the `/themes/terrassa` directory and make your commits there.
2. Go back into the root directory `lernenwiedieprofis.ch` add commit the `/themes/terrassa` folder (git will treat it as a pointer to a commit in the submodule repo).

**Last**, push your changes to the remote git repository (or repositories in case you adapted the submodule).

- If you're a collaborator (with write access) in the project already, you can either `git push` to `master` directly (if you feel confident) or create a new branch and start a pull request otherwise. 
- If you're not a collaborator yet, but have an account at [git.fsfe.org](git.fsfe.org), please [let me know that you'd like to join the project](mailto:alexander.pitsch@fsfe.org). 
- If you don't have (and don't want to have) an account at [git.fsfe.org](git.fsfe.org), you can still contribute by using an email based git workflow; please have a look at [this tutorial](https://git-send-email.io/) to that end.

## License

[GPL-3.0-only](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Contact

[Ralf Hersel](mailto:ralfhersel@fsfe.org)
