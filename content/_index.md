---
title: "Ziele"
description: "Warum Freie Software an Schulen nützt"
images: ["title_background.svg"]
draft: false
menu: main
weight: 1
---

# Lernen wie die Profis

## Die Kampagne der «FSFE-Lokalgruppe Zürich»
informiert über Digitale Souveränität,  
erklärt professionelle Ansätze und  
bietet konkrete Vorschläge.

{{< rawhtml >}}
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
{{< /rawhtml >}}
