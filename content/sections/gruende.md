---
title : "Ziele"
description: "Warum ist Freie Software an Schulen wichtig?"
images: []
draft: false
weight: 1
---


Ziel der Kampagne
========================================================================

Aus dem Schweizer {{< link href="https://www.lehrplan21.ch/" title="Lehrplan21" >}} geht hervor, dass Schülerinnen den Aufbau und die Funktionsweise von informationsverarbeitenden Systemen kennen sollen und Konzepte der sicheren Datenverarbeitung anwenden können. Leider wird an Schulen vorwiegend mittels unfreier Software gelehrt. Diese bietet den Schülerinnen und Lehrpersonen keine Möglichkeit, die Funktionsweise und Grundsätze zu studieren, sondern erlaubt nur eine Einsicht in die Benutzung aus Anwendersicht. Primäres Ziel des Lehrplan21 ist jedoch nicht, dass Produktwissen vermittelt wird. Aus diesem Grunde fordern wir, dass nicht nur der Umgang mit proprietären Systemen (Microsoft, Apple, etc.) unterrichtet wird, sondern alternative Ansätze aufgezeigt werden und die digitale Mündigkeit und Souveränität der Schülerinnen, aber auch der Lehrpersonen mittels Freier Software gefördert werden.

{{< imgcenter src="/images/greenline.png" >}}

Digitale Souveränität
========================================================================

Unter digitaler Souveränität versteht man selbstbestimmtes Handeln und die eigene Kontrolle bezüglich der Verwendung von digitalen Medien. Digitale Souveränität ist die weitgehende Kontrolle einer Schülerin, Lehrerin oder Schule über die Daten, die sie auf lokalen oder Online-Plattformen generiert und mit denen sie arbeitet. Der Verlust der Kontrolle über Daten stellt ein grosses Risiko für unser Bildungswesen dar. Schülerinnen, Lehrpersonal und Schulen werden überwacht und ihre Daten werden von Unternehmen verkauft.

‣ Stellt euch vor, jemand würde auf dem Pausenplatz Drogen verkaufen um zukünftige Abhängigkeiten zu erzeugen.

‣ Stellt euch vor, Fast-Food Ketten würden Schulen unterstützen um ihre Idee von gesundem Essen zu verbreiten.

Schulen unterliegen strengen Kriterien bezüglich der aktiven Beeinflussung durch Unternehmen, z.B. durch Werbung und Gratisabgaben. Unsere Schulen sollen kein Marktplatz sein um Neukunden zu generieren. Schülerinnen sind schützenswerte Personen, gerade aus wirtschaftlicher und kultureller Sicht.

Das Vorgehen der IT-Konzerne an Schulen ist radikal. Mit Lockvogel-Angeboten werden zukünftige Kunden so früh wie möglich in eine Abhängigkeit gezwungen, aus der es später kaum ein Entkommen gibt. Die bessere Lösung für unsere Schulen ist Freie Software. Diese bietet neben der Rechtssicherheit die entscheidenden vier Freiheiten:

‣ **Verwenden** - Freie Software darf uneingeschränkt von allen und zu jedem Zweck verwendet werden

‣ **Verstehen** - Freie Software ist quelloffen und kann von allen eingesehen und studiert werden

‣ **Verteilen** - Freie Software kann beliebig kopiert und an andere verteilt werden

‣ **Verbessern** - Freie Software kann von allen verändert und verbessert werden

Die vier Freiheiten sind ideal auf die Grundsätze und Bedürfnisse von Schulen zugeschnitten. Lehrerinnen müssen nicht fürchten, mit dem Gesetz in Konflikt zu geraten und Schülerinnen können auf die Wahrung ihrer Privatsphäre vertrauen.

{{< imgcenter src="/images/greenline.png" >}}

Voraussetzungen schaffen
========================================================================

Digitale Souveränität an Schulen geschieht weder von alleine noch über Nacht. Hierfür sind Einsicht, Mut, Ressourcen und politische Rückendeckung gefragt. Ohne den Willen aller Beteiligten wandeln sich unsere Schulen immer mehr zu Brutstätten kommerzieller Begehrlichkeiten. Am Anfang jedes Wandels steht der Wille zur Veränderung - bei Schulbehörden, Lehrerinnen und Schülerinnen.

Freie Software sollte aus ethischen Gründen bevorzugt zum Einsatz kommen. Sie bereitet die Schülerinnen darauf vor, in einer offenen digitalen Gesellschaft zu leben. Zudem fördert sie die Neugier, denn sie erlaubt die Funktionsweise von Software zu erlernen. Proprietäre Software hindert jedoch diesen Wissensdurst, denn der Code ist nicht einsehbar und kann nicht an Mitschülerinnen weitergegeben werden.

Leider genügt der Hinweis auf die ethischen Aspekte von Software nicht, dass sich dieses Paradigma in den Schulen durchsetzt. Und es genügt auch nicht der Einkauf eines Sorglospaketes an Geräten und Programmen. In erster Linie sollte in die Ausbildung des Lehrpersonals investiert werden, optimal bereits an pädagogischen Hochschulen, damit in Zukunft die Anwenderinnen gut betreut werden können.

Kaufen sie nicht unreflektiert was bei anderen Schulen bereits im Einsatz ist. Eine seriöse Auswahl und Bewertung von Soft- und Hardware ist immer von Nöten um den Schülerinnen ein gutes Ausbildungsangebot zu gewährleisten.

In der Realität wird stattdessen meist der Weg des geringsten Widerstands gewählt. Die globalen IT-Firmen scheuen keine Kosten um Schulen von ihren geschlossenen Lösungen zu überzeugen. Da die Informatik nicht zur Kernkompetenz von Schulen und Schulbehörden gehört, lassen sich viele Entscheidungsträger gerne an die Hand nehmen. Als Resultat fliesst ein Grossteil unserer Steuergelder zu den globalen IT-Anbietern, unter Verlust der digitalen Selbstbestimmung.

Jedes KMU hat heute eine oder mehrere IT-Mitarbeitende, die ihr Fach gelernt haben und den reibungslosen Einsatz der Investitionen unterstützen. IT an Schulen darf nicht das Hobby einer Lehrperson sein. Durch den Zusammenschluss von Schulen auf Gemeindeebene und die Unterstützung durch eine lokale IT-Firma kann viel Last abgegeben und an Professionalität gewonnen werden. Dadurch wird auch die lokale Wirtschaft nachhaltig gestützt.

Der Wille zur Veränderung ist die beste Voraussetzung um Schülerinnen den Weg in eine unabhängige IT-Zukunft zu ebnen.

{{< imgcenter src="/images/greenline.png" >}}

Lösungen für die Praxis
========================================================================

Schulen brauchen keine theoretischen Erklärungen, sondern praktische Vorschläge und Ideen. Nichtsdestotrotz ist das Verständnis der Situation und der Zusammenhänge wichtig für richtige Entscheidungen. In unserem Kapitel [Lösungen](loesungen/) haben wir viele konkrete Vorschläge beschrieben, wie der Schulbetrieb mit Freier Software effizient und einfach gestaltet werden kann. Im Kapitel [Praxis](praxis/) finden sich Beispiele von Schulen, die den Wechsel erfolgreich vollzogen haben.

{{< imgcenter src="/images/greenline.png" >}}

Lernen wie die Profis
========================================================================

Profis arbeiten mit Freier Software, weil diese die Kreativität, den Forschungsdrang und die wissenschaftlichen Prinzipien unterstützt, statt sie zu behindern. Prominente Beispiele dafür sind das {{< link href="https://home.cern/news/news/computing/migrating-open-source-technologies" title="CERN" >}} bei Genf sowie diverse Universitäten (Lausanne, Leibzig, Hannover, Zürich, Bern, ..). Auch in der Software-Entwicklung ist der Einsatz Freier Software 'state of the art'. Lehrerinnen können Schüler und Studentinnen nur dann optimal auf das Berufsleben vorbereiten, wenn sie 'Lehren und Lernen wie die Profis'.
