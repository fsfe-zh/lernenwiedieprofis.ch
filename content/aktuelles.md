---
title: "Aktuelles"
description: ""
images: []
draft: false
menu: main
weight: 2
---

Diese Seite gibt eine Übersicht zu aktuellen Entwicklungen und
Neuigkeiten zum Einsatz Freier Software an Schulen.

- 30.09.24: [gnulinux.ch: Linux-Tablet StarLite als Unterrichtsgerät][gnulinux starlite]
- 17.09.24: [gnulinux.ch: Schriften][gnulinux schriften]
- 05.09.24: [gnulinux.ch: LaTeX und Xournal++ im Mathe-Unterricht, Teil 1][gnulinux latex xournal teil1]
- 18.08.24: [openeducationday.ch: SAVE THE DATE für 2025][openeducationday 2025]
- 08.08.24: [gnulinux.ch: Freie Software in der Schule: Einführung in Linux für eine Schulklasse][gnulinux gymizuerich]
- 23.07.24: [gnulinux.ch: Freie Software in der Schule: Warum ich meine eigene Notenverwaltung schreibe][gnulinux notenverwaltung]
- 08.07.24: [gnulinux.ch Freie Software in der Schule: Lizenzen für Dokumente und Werke][gnulinux lizenzen]
- 11.06.24: [gnulinux.ch: Freie Software in der Schule: Ein OneNote
  sie zu knechten][gnulinux onenote]
- 04.06.24: [NOYB reicht in Österreich Beschwerde gegen Microsoft ein,
  weil der Einsatz von Microsoft 365 Education Datenschutzrechte von Kindern
  verletzt][noyb AU MS365]

[gnulinux starlite]: https://gnulinux.ch/linux-tablet-starlite-als-unterrichtsgeraet
[gnulinux schriften]: https://gnulinux.ch/freie-software-in-der-schule-schriften 
[gnulinux latex xournal teil1]: https://gnulinux.ch/freie-software-in-der-schule-latex-und-xournalpp-im-unterricht
[openeducationday 2025]: https://openeducationday.ch
[gnulinux gymizuerich]: https://gnulinux.ch/freie-software-in-der-schule-einfuehrung-in-linux 
[gnulinux notenverwaltung]: https://gnulinux.ch/freie-software-in-der-schule-warum-ich-meine-eigene-notenverwaltung-schreibe
[gnulinux lizenzen]: https://gnulinux.ch/freie-software-in-der-schule-lizenzen-und-schule   
[gnulinux onenote]: https://gnulinux.ch/freie-software-in-der-schule-ein-onenote-sie-zu-knechten
[noyb AU MS365]: https://noyb.eu/de/microsoft-violates-childrens-privacy-blames-your-local-school

